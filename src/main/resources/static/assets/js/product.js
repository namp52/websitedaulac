var app = angular.module("ProductManagement", ["ngSanitize"]);

// Controller Part
app.controller("ProductController", function($scope, $http) {

    $scope.carts = [];

    $scope.amount="";
    $scope.total=0;

    _refreshCartData();


    function _refreshCartData() {
        $('#cart1').hide();
        $('#cart2').hide();
        $('#cart3').hide();
        $('#cart4').hide();
        $scope.total=0;
        $http({
            method: 'GET',
            url: '/api/gio-hang'
        }).then(
            function(res) { // success
                $scope.carts = res.data;
                for(var i=0;i<$scope.carts.length;i++){
                    $scope.total= $scope.total+ ($scope.carts[i].product.price*$scope.carts[i].quantity);
                }
                $scope.amount=$scope.carts.length;
                if($scope.carts.length>0){
                    $('#cart1').show();
                    $('#cart3').show();

                }else{
                    $('#cart2').show();
                    $('#cart4').show();

                }
            },
            function(res) { // error
                console.log("Error: " + res.status + " : " + res.data);
            }
        );
    }

    $scope.addQuantity = function(p){
        $http({
            method: 'GET',
            url: '/api/cart/addQuantity/' +p.product.id
        }).then(
            function(res) { // success
               _refreshCartData();
            },
            function(res) { // error
                console.log("Error: " + res.status + " : " + res.data);
            }
        );
    }
    $scope.removeQuantity = function(p){
        $http({
            method: 'GET',
            url: '/api/cart/removeQuantity/' +p.product.id
        }).then(
            function(res) { // success
                _refreshCartData();
            },
            function(res) { // error
                console.log("Error: " + res.status + " : " + res.data);
            }
        );
    }
    $scope.remove = function(p){
        $http({
            method: 'GET',
            url: '/api/cart/removeCart/' +p.product.id
        }).then(
            function(res) { // success
                _refreshCartData();
            },
            function(res) { // error
                console.log("Error: " + res.status + " : " + res.data);
            }
        );
    }
    $("#formcheckout").validate({
        rules: {
            name: {
                minlength: 2,
                maxlength: 16,
                required: true
            },
            email: {
                minlength: 6,
                maxlength: 25,
                required: true,
                email :true
            },
            phone: {
                minlength: 10,
                maxlength: 13,
                required: true
            },
            address: {
                minlength: 6,
                maxlength: 50,
                required: true
            }
        },
        messages: {
            name: {
                minlength: "Tên của bạn không được nhỏ hơn 2 ký tự ",
                maxlength: "Tên của bạn không được dài quá 25 ký tự",
                required: "Vui lòng nhập họ tên của bạn"
            },
            email: {
                minlength: "Email không được nhỏ hơn 6 ký tự ",
                maxlength: "Emai không được dài quá 15 ký tự",
                required: "Vui lòng nhập email của bạn",
                email : "Địa chỉ email không đúng"
            },
            phone: {
                minlength: "Số điện thoại không được nhỏ hơn 10 ký tự",
                maxlength: "Số điện thoại không được quá 13 ký tự",
                required: "Vui lòng nhập số điện thoại của bạn"
            },
            address: {
                minlength: "Địa chỉ không được nhỏ hơn 6 ký tự",
                maxlength: "Địa chỉ  không được quá 50 ký tự",
                required: "Vui lòng nhập địa chỉ của bạn"
            }

        },
        highlight: function(element) {
            $(element).removeClass('is-valid').addClass('is-invalid');
        },
        unhighlight: function(element) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        },
        submitHandler: function(form) {
            form.submit();
            alert("Đặt hàng thành công vui lòng kiểm tra email của bạn")
        }
    });
    $("#formreview").validate({
        rules: {
            name: {
                minlength: 2,
                maxlength: 16,
                required: true
            },
            email: {
                minlength: 6,
                maxlength: 25,
                required: true,
                email :true
            },
            content: {
                minlength: 10,
                maxlength: 250,
                required: true
            },

        },
        messages: {
            name: {
                minlength: "Tên của bạn không được nhỏ hơn 2 ký tự ",
                maxlength: "Tên của bạn không được dài quá 25 ký tự",
                required: "Vui lòng nhập họ tên của bạn"
            },
            email: {
                minlength: "Email không được nhỏ hơn 6 ký tự ",
                maxlength: "Emai không được dài quá 15 ký tự",
                required: "Vui lòng nhập email của bạn",
                email : "Địa chỉ email không đúng"
            },
            content: {
                minlength: "Nội dung đánh giá không được nhỏ hơn 10 ký tự",
                maxlength: "Nội dung đánh giá không được quá 255 ký tự",
                required: "Vui lòng nhập đánh giá của bạn"
            },

        },
        highlight: function(element) {
            $(element).removeClass('is-valid').addClass('is-invalid');
        },
        unhighlight: function(element) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        },
        submitHandler: function(form) {
            form.submit();
            alert("Đánh giá thành công vui. Rất cảm ơn quý khách.")
        }
    });
    $("#formcontact").validate({
        rules: {
            name: {
                minlength: 2,
                maxlength: 16,
                required: true
            },
            email: {
                minlength: 6,
                maxlength: 25,
                required: true,
                email :true
            },
            content: {
                minlength: 10,
                maxlength: 250,
                required: true
            },
            subject: {
                minlength: 10,
                maxlength: 30,
                required: true
            }


        },
        messages: {
            name: {
                minlength: "Tên của bạn không được nhỏ hơn 2 ký tự ",
                maxlength: "Tên của bạn không được dài quá 25 ký tự",
                required: "Vui lòng nhập họ tên của bạn"
            },
            email: {
                minlength: "Email không được nhỏ hơn 6 ký tự ",
                maxlength: "Emai không được dài quá 15 ký tự",
                required: "Vui lòng nhập email của bạn",
                email : "Địa chỉ email không đúng"
            },
            content: {
                minlength: "Nội dung đánh giá không được nhỏ hơn 10 ký tự",
                maxlength: "Nội dung đánh giá không được quá 250 ký tự",
                required: "Vui lòng nhập đánh giá của bạn"
            },
            subject: {
                minlength: "Tiêu đề không được nhỏ hơn 10 ký tự",
                maxlength: "Tiêu đề  không được quá 30 ký tự",
                required: "Vui lòng nhập tiêu đề"
            }

        },
        highlight: function(element) {
            $(element).removeClass('is-valid').addClass('is-invalid');
        },
        unhighlight: function(element) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        },
        submitHandler: function(form) {
            form.submit();
            alert("Đánh giá thành công vui. Rất cảm ơn quý khách.")
        }
    });

    $("#formsearch").validate({
        rules: {
            search: {
                maxlength: 25,
                required: true,
            }
        },
        messages: {
            search: {
                maxlength: "Nội dung không được dài quá 15 ký tự",
                required: "Vui lòng nhập nội dung tìm kiếm của bạn",
            },
        },
        highlight: function(element) {
            $(element).removeClass('is-valid').addClass('is-invalid');
        },
        unhighlight: function(element) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        }
    });

    $('input.qty').bind('keypress', function(e) {
        return ( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57)) ? false : true ;
    })
    $('#formemail').submit(function() {

        alert("Chúng tôi sẽ gửi các bản tin về các chương trình khuyến mại trong email của bạn")
        return true;
    });
    $("#cm").validate({
        rules: {
            name: {
                minlength: 2,
                maxlength: 16,
                required: true
            },
            email: {
                minlength: 6,
                maxlength: 25,
                required: true,
                email :true
            },
            content: {
                minlength: 10,
                maxlength: 250,
                required: true
            },

        },
        messages: {
            name: {
                minlength: "Tên của bạn không được nhỏ hơn 2 ký tự ",
                maxlength: "Tên của bạn không được dài quá 25 ký tự",
                required: "Vui lòng nhập họ tên của bạn"
            },
            email: {
                minlength: "Email không được nhỏ hơn 6 ký tự ",
                maxlength: "Emai không được dài quá 15 ký tự",
                required: "Vui lòng nhập email của bạn",
                email : "Địa chỉ email không đúng"
            },
            content: {
                minlength: "Nội dung đánh giá không được nhỏ hơn 10 ký tự",
                maxlength: "Nội dung đánh giá không được quá 255 ký tự",
                required: "Vui lòng nhập đánh giá của bạn"
            },

        },
        highlight: function(element) {
            $(element).removeClass('is-valid').addClass('is-invalid');
        },
        unhighlight: function(element) {
            $(element).removeClass('is-invalid').addClass('is-valid');
        },
        submitHandler: function(form) {
            form.submit();
            alert("Đánh giá thành công vui. Rất cảm ơn quý khách.")
        }
    });


});