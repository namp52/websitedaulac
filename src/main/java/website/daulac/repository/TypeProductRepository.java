package website.daulac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import website.daulac.entity.TypeProduct;
@Repository
public interface TypeProductRepository  extends JpaRepository<TypeProduct,Integer> {
}
