package website.daulac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import website.daulac.entity.BillDetail;

import java.util.List;

@Repository
public interface BillDetailRepository  extends JpaRepository<BillDetail,Integer> {
    List<BillDetail> findByBill_Id(Integer id);
}
