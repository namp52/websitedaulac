package website.daulac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import website.daulac.entity.Comment;
@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {
    Long countByBlog_Id(Integer id);
}
