package website.daulac.repository;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import website.daulac.entity.Blog;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public interface BlogRepository extends JpaRepository<Blog,Integer> {
  Blog findByUrl(String url);

}
