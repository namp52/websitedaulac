package website.daulac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import website.daulac.entity.Review;

import java.util.List;
@Repository
public interface ReviewRepository extends JpaRepository<Review,Integer> {
    List<Review> findByProduct_Id(Integer id);
}
