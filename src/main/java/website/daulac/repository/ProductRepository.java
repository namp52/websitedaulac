package website.daulac.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import website.daulac.entity.Product;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product,Integer> {
    Product findByUrl(String url);
    List<Product> findByTypeProduct_Name(String name);
    List<Product> findByTypeProduct_Id(Integer id);
}
