package website.daulac.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;
import website.daulac.entity.*;
import website.daulac.model.Item;
import website.daulac.model.ProductDto;
import website.daulac.service.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class Rescontroller {
    @Autowired
    private ProductService productService;
    @Autowired
    private TypeProductService typeProductService;
    @Autowired
    private BillService billService;
    @Autowired
    BillDetaillService billDetaillService;
    @Autowired
    BlogService blogService;

    @GetMapping("/api/allProduct")
    public List<Product> getAllProduct() {
        return productService.getListAllProdduct();
    }

    @GetMapping("/api/allTypeProduct")
    public List<TypeProduct> getAllTypeProduct() {
        return typeProductService.getAllTypeProduct();
    }

    @GetMapping("/api/allBill")
    public List<Bill> getAllBill() {
        return billService.getAllBill();
    }

    @GetMapping("/api/billDetail/{id}")
    public List<BillDetail> getBillDetail(@PathVariable("id") Integer id) {
        return billDetaillService.getListBillDetail(id);
    }
    @GetMapping("/api/gio-hang")
    public List<Item> getListItem(HttpSession session) {
        List<Item> cart = (List<Item>) session.getAttribute("cart");
        return cart;
    }
    @GetMapping("/api/allBlog")
    public List<Blog> getListBlog() {
        List<Blog> list =blogService.getListBlog();
        return list;
    }
    @GetMapping("/api/cart/addQuantity/{id}")
    public void addQuantity(@PathVariable("id") Integer id, HttpSession session) {
        List<Item> cart = (List<Item>) session.getAttribute("cart");
        for (int i = 0; i < cart.size(); i++) {
            if (cart.get(i).getProduct().getId() == id) {
                int quantity = cart.get(i).getQuantity() + 1;
                cart.get(i).setQuantity(quantity);
            }
        }
        session.setAttribute("cart", cart);
    }
    @GetMapping("/api/cart/removeQuantity/{id}")
    public void removeQuantity(@PathVariable("id") Integer id, HttpSession session) {
        List<Item> cart = (List<Item>) session.getAttribute("cart");
        for (int i = 0; i < cart.size(); i++) {
            if (cart.get(i).getProduct().getId() == id) {

                int quantity = cart.get(i).getQuantity() - 1;
                if (quantity > 0) {
                    cart.get(i).setQuantity(quantity);
                }

            }
        }
        session.setAttribute("cart", cart);
    }
    @GetMapping("/api/cart/removeCart/{id}")
    public void removeCart(@PathVariable("id") Integer id, HttpSession session) {
        List<Item> cart = (List<Item>) session.getAttribute("cart");
        int index = -1;
        for (int i = 0; i < cart.size(); i++) {
            if (cart.get(i).getProduct().getId() == id) {
                index = i;
                break;
            }
        }
        cart.remove(index);
        session.setAttribute("cart", cart);
    }

    @PostMapping(value ="/api/saveProduct")
    @ResponseBody
    public void saveProduct(@Valid Product product) {
                productService.saveProduct(product);
    }

   @PutMapping(value ="/api/updateProduct")
    @ResponseBody
    public void updateProduct(@Valid Product product) {
        productService.saveProduct(product);
    }


    @DeleteMapping(value = "/api/deleteProduct/{id}")
    public void deleteProductt(@PathVariable("id") Integer id) {
        productService.deleteProduct(productService.getProductId(id).get());
    }

    @PostMapping(value = "/api/saveTypeProduct")
    @ResponseBody
    public void saveTypeProduct(@Valid TypeProduct typeProduct) {
        typeProductService.saveTypeProduct(typeProduct);
    }
    @PostMapping(value = "/api/saveBlog")
    public void addBlog(@Valid Blog blog){
        blog.setDate(new Date());
        blogService.saveBlog(blog);
    }
    @DeleteMapping(value = "/api/deleteBlog/{id}")
    public void deleteBlog(@PathVariable("id") Integer id){
        blogService.deleteBlog(blogService.getBlog(id).get());

    }
    @PutMapping(value = "/api/updateBlog")
    public void updateBlog(@Valid Blog blog){
        blogService.saveBlog(blog);
    }

    @PutMapping(value = "/api/updateTypeProduct")
    @ResponseBody
    public void updateTypeProduct(@Valid TypeProduct typeProduct) {
        typeProductService.saveTypeProduct(typeProduct);
    }

    @DeleteMapping(value = "/api/deleteTypeProduct/{id}")
    public void deleteTypeProductt(@PathVariable("id") Integer id) {
        typeProductService.deleteTypeProduct(typeProductService.getTypeProductId(id).get());
    }

    public File getFolderUpload() {
        File folderUpload = new File(System.getProperty("user.dir") + "/src/main/resources/static/uploadmedia/images");
        if (!folderUpload.exists()) {
            folderUpload.mkdirs();
        }
        return folderUpload;
    }
}
