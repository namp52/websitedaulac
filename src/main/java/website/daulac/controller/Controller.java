package website.daulac.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;
import website.daulac.entity.*;
import website.daulac.model.Item;
import website.daulac.repository.BlogRepository;
import website.daulac.repository.CommentRepository;
import website.daulac.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@org.springframework.stereotype.Controller
public class Controller  {
    @Autowired private ProductService productService;
    @Autowired private ReviewService reviewService;
    @Autowired private BillService billService;
    @Autowired private BillDetaillService billDetaillService;
    @Autowired private TypeProductService typeProductService;
    @Autowired private BlogService blogService;
    @Autowired private BlogRepository blogRepository;
    @Autowired private CommentRepository commentRepository;
    @GetMapping(value = "/")
    public String getIndex(ModelMap modelMap){
        modelMap.addAttribute("product",productService.getListAllProdduct());
        modelMap.addAttribute("blog", blogRepository.findAll(PageRequest.of(0, 3, Sort.by("id").descending())));

        return "index";
    }
    @GetMapping(value = "/gio-hang")
    public String getCart(ModelMap modelMap){
        return "cart";
    }
    @GetMapping(value = "/lien-he")
    public String getContact(ModelMap modelMap){
        return "contact";
    }
    @GetMapping(value = "/gioi-thieu")
    public String getAbout(ModelMap modelMap){
        return "about";
    }

    @GetMapping(value = "/blog")
    public String getBlog(ModelMap modelMap){
        modelMap.addAttribute("blog" , blogService.getListBlog());
        return "blog";
    }
    @GetMapping(value = "/blog/{id}")
    public String getBlogDetail(@PathVariable("id") String id,ModelMap modelMap){
        Blog blog=blogService.getListBlogUrl(id);
        Comment comment =new Comment();
        comment.setBlog(blog);
        modelMap.addAttribute("comment",comment);
        modelMap.addAttribute("allComment",commentRepository.findAll());
        modelMap.addAttribute("count",commentRepository.countByBlog_Id(blog.getId()));
        modelMap.addAttribute("blog" ,blog);
        modelMap.addAttribute("blog2", blogRepository.findAll(PageRequest.of(0, 3, Sort.by("id").descending())));
        return "blogDetail";
    }
    @PostMapping(value = "/addComment")
    public String addComment(@Valid Comment comment){
        comment.setDate(new Date());
        commentRepository.save(comment);
        return "redirect:/blog/" +comment.getBlog().getUrl();
    }
    @GetMapping(value = "/tin-tuc/{id}")
    public String getBlogDetail(ModelMap modelMap,@PathVariable("id")String id){
        return "blogDetail";
    }
    @GetMapping(value = "/gio-hang/thanh-toan")
    public String getCheckOut(ModelMap modelMap){
        modelMap.addAttribute("bill",new Bill());
        return "checkout";
    }
    @PostMapping(value = "/addBill")
    public String oder(ModelMap modelMap,@Valid Bill bill,HttpSession session){
        List<Item> cart = (List<Item>) session.getAttribute("cart");
        float total=0;
        for(int i=0; i<cart.size();i++) {
                total+=cart.get(i).getProduct().getPrice()*cart.get(i).getQuantity();
        }
        bill.setDate(new Date());
        bill.setTotal(total);
        bill.setStatus("Chưa xác nhận");
        billService.saveBill(bill);

        for(int i=0; i<cart.size();i++) {
            BillDetail billDetail = new BillDetail();
            billDetail.setBill(bill);
            billDetail.setAmount(cart.get(i).getQuantity());
            billDetail.setProduct(cart.get(i).getProduct());
            billDetaillService.saveBillDetail(billDetail);
        }
        session.removeAttribute("cart");
        return "redirect:/";
    }
    @GetMapping(value = "/san-pham")
    public String getProduct(ModelMap modelMap){
        modelMap.addAttribute("product",productService.getListAllProdduct());
        modelMap.addAttribute("typeProduct",typeProductService.getAllTypeProduct());
        return "product";
    }
    @GetMapping(value = "/loai-san-pham/{id}")
    public String getProduct(ModelMap modelMap,@PathVariable("id") Integer id){
        modelMap.addAttribute("product",productService.getListProductType(id));
        modelMap.addAttribute("typeProduct",typeProductService.getAllTypeProduct());
        return "product";
    }
    @GetMapping(value = "/san-pham/{id}")
    public String getProductDetail(@PathVariable("id") String url,ModelMap modelMap){
        Product product=productService.getProductUrl(url);
        modelMap.addAttribute("product",product);
        Item item = new Item();
        item.setQuantity(1);
        item.setProduct(product);
        modelMap.addAttribute("item", item);
        modelMap.addAttribute("review",reviewService.getListReivewByProductId(product.getId()));
        List<Product> products =productService.getListProductType(product.getTypeProduct().getId());
        int index=-1;
        for(int i=0; i<products.size();i++){
            if(products.get(i).getId()==product.getId()){
                index=i;
                break;
            }
        }
        products.remove(index);
        modelMap.addAttribute("products",products);
        Review review= new Review();
        review.setProduct(product);
        modelMap.addAttribute("review2",review);
        return "productDetail";
    }
    @PostMapping(value = "/add")
    public String addReviews(@Valid Review review){
        review.setDate(new Date());
        reviewService.saveReview(review);
        return "redirect:/san-pham/"+ review.getProduct().getUrl();
    }
    @PostMapping(value = "/addToCart")
    public String addToCart(@Valid Item item,HttpSession session){
        System.out.println(item.getProduct().getId());
        if (session.getAttribute("cart") == null) {
                List<Item> cart = new ArrayList<Item>();
            cart.add(new Item(item.getProduct(), item.getQuantity()));
            session.setAttribute("cart", cart);
        } else {
            List<Item> cart = (List<Item>) session.getAttribute("cart");
            int index = this.exists(item.getProduct().getId(), cart);
            if (index == -1) {
                cart.add(new Item(item.getProduct(), item.getQuantity()));
            } else {
                int quantity = cart.get(index).getQuantity() + item.getQuantity();
                cart.get(index).setQuantity(quantity);
            }
            session.setAttribute("cart", cart);
        }
        return "redirect:/gio-hang";
    }
    @GetMapping(value = "/gio-hang/{id}")
    public String getBuy(@PathVariable("id") Integer id, ModelMap modelMap, HttpSession session){
        if (session.getAttribute("cart") == null) {
            List<Item> cart = new ArrayList<Item>();
            cart.add(new Item(productService.getProductId(id).get(), 1));
            session.setAttribute("cart", cart);
            System.out.println(cart.get(0).getProduct().getName());
        } else {
            List<Item> cart = (List<Item>) session.getAttribute("cart");
            int index = this.exists(id, cart);
            if (index == -1) {
                cart.add(new Item(productService.getProductId(id).get(), 1));
            } else {
                int quantity = cart.get(index).getQuantity() + 1;
                cart.get(index).setQuantity(quantity);
            }
            session.setAttribute("cart", cart);
        }

        return "redirect:/gio-hang";
    }
    private int exists(int id, List<Item> cart) {
        for (int i = 0; i < cart.size(); i++) {
            if (cart.get(i).getProduct().getId() ==id) {
                return i;
            }
        }
        return -1;
    }
    @GetMapping(value = "/admin")
    public String getAdmin(){
        return "admin";
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ModelAndView handleError404(HttpServletRequest request, Exception e)   {
        Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Request: " + request.getRequestURL() + " raised " + e);
        return new ModelAndView("404");
    }
}
