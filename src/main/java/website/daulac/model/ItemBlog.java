package website.daulac.model;

import website.daulac.entity.Blog;

import java.util.ArrayList;
import java.util.List;

public class ItemBlog {
    private Blog blog;
    private Long count;

    public ItemBlog(Blog blog, Long countByBlog_id) {
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }


}
