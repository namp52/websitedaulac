package website.daulac.service;

import website.daulac.entity.Review;

import java.util.List;

public interface ReviewService {
    List<Review> getListReivewByProductId(Integer id) ;
    void saveReview(Review review);
}
