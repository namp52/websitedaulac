package website.daulac.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import website.daulac.entity.Bill;
import website.daulac.repository.BillRepository;
import website.daulac.service.BillService;

import java.util.List;

@Service
public class BillServiceImpl implements BillService {
    @Autowired private BillRepository billRepository;
    @Override
    public void saveBill(Bill bill) {
        billRepository.save(bill);

    }

    @Override
    public List<Bill> getAllBill() {
        return billRepository.findAll();
    }
}
