package website.daulac.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import website.daulac.entity.Review;
import website.daulac.repository.ReviewRepository;
import website.daulac.service.ReviewService;

import java.util.List;
@Service
public class ReviewServiceImpl implements ReviewService {
    @Autowired private ReviewRepository reviewRepository;
    @Override
    public List<Review> getListReivewByProductId(Integer id) {
        return reviewRepository.findByProduct_Id(id);
    }

    @Override
    public void saveReview(Review review) {
        reviewRepository.save(review);
    }
}
