package website.daulac.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import website.daulac.entity.BillDetail;
import website.daulac.repository.BillDetailRepository;
import website.daulac.service.BillDetaillService;

import java.util.List;

@Service
public class BillDetailServiceImpl implements BillDetaillService {
    @Autowired private BillDetailRepository billDetailRepository;
    @Override
    public void saveBillDetail(BillDetail billDetail) {
            billDetailRepository.save(billDetail);
    }

    @Override
    public List<BillDetail> getListBillDetail(Integer id) {
        return billDetailRepository.findByBill_Id(id);
    }
}
