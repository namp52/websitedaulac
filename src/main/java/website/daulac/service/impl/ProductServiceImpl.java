package website.daulac.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import website.daulac.entity.Product;
import website.daulac.repository.ProductRepository;
import website.daulac.service.ProductService;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired private ProductRepository productRepository;
    @Override
    public List<Product> getListAllProdduct() {
        return productRepository.findAll();
    }

    @Override
    public Optional<Product> getProductId(Integer id) {
        return productRepository.findById(id);
    }

    @Override
    public Product getProductUrl(String url) {
        return productRepository.findByUrl(url);
    }

    @Override
    public List<Product> getListProductType(Integer id) {
        return productRepository.findByTypeProduct_Id(id);
    }

    @Override
    public List<Product> getListProductTypeName(String id) {
        return productRepository.findByTypeProduct_Name(id);
    }

    @Override
    public void saveProduct(Product product) {
        product.setTypeProduct(product.getTypeProduct());
        productRepository.save(product);
    }

    @Override
    public void deleteProduct(Product product) {
        productRepository.delete(product);
    }
}
