package website.daulac.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import website.daulac.entity.TypeProduct;
import website.daulac.repository.TypeProductRepository;
import website.daulac.service.TypeProductService;

import java.util.List;
import java.util.Optional;

@Service
public class TypeProductServiceImpl implements TypeProductService {
    @Autowired private TypeProductRepository typeProductRepository;
    @Override
    public List<TypeProduct> getAllTypeProduct() {
        return typeProductRepository.findAll();
    }

    @Override
    public Optional<TypeProduct> getTypeProductId(Integer id) {
        return typeProductRepository.findById(id);
    }

    @Override
    public void saveTypeProduct(TypeProduct typeProduct) {
        typeProductRepository.save(typeProduct);
    }

    @Override
    public void deleteTypeProduct(TypeProduct typeProduct) {
        typeProductRepository.delete(typeProduct);
    }
}
