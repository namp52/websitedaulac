package website.daulac.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import website.daulac.entity.Blog;
import website.daulac.model.ItemBlog;
import website.daulac.repository.BlogRepository;
import website.daulac.repository.CommentRepository;
import website.daulac.service.BlogService;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BlogServiceImpl implements BlogService {
    @Autowired private BlogRepository blogRepository;
    @Autowired private CommentRepository  commentRepository;

    @Override
    public List<Blog> getListBlog() {
        return blogRepository.findAll();
    }

    @Override
    public Optional<Blog> getBlog(Integer id) {
        return blogRepository.findById(id);
    }

    @Override
    public void saveBlog(Blog blog) {
        blogRepository.save(blog);
    }

    @Override
    public void deleteBlog(Blog blog) {
        blogRepository.delete(blog);

    }

    @Override
    public Blog getListBlogUrl(String url) {
        return blogRepository.findByUrl(url);
    }

    @Override
    public List<ItemBlog> getListItem(Page<Blog> list) {
        List<ItemBlog> itemBlogs = new ArrayList<>();
        for(Blog blog : list){
            itemBlogs.add(new ItemBlog(blog,commentRepository.countByBlog_Id(blog.getId())));
        }
        return itemBlogs;
    }
}
