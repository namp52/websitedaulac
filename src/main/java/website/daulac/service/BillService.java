package website.daulac.service;

import website.daulac.entity.Bill;

import java.util.List;

public interface BillService {
    void saveBill(Bill bill);
    List<Bill> getAllBill();
}
