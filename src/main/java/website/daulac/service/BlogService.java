package website.daulac.service;

import org.springframework.data.domain.Page;
import website.daulac.entity.Blog;
import website.daulac.model.ItemBlog;

import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;

public interface BlogService {
    List<Blog> getListBlog();
    Optional<Blog> getBlog(Integer id);
    void saveBlog(Blog blog);
    void deleteBlog(Blog blog);
    Blog getListBlogUrl(String url);
    List<ItemBlog> getListItem(Page<Blog> list);

}
