package website.daulac.service;

import website.daulac.entity.BillDetail;

import java.util.List;

public interface BillDetaillService {
    void saveBillDetail(BillDetail billDetail);
    List<BillDetail> getListBillDetail(Integer id);
}
