package website.daulac.service;

import website.daulac.entity.TypeProduct;

import java.util.List;
import java.util.Optional;

public interface TypeProductService {
    List<TypeProduct> getAllTypeProduct();
    Optional<TypeProduct> getTypeProductId(Integer id);
    void saveTypeProduct(TypeProduct typeProduct);
    void deleteTypeProduct(TypeProduct typeProduct);
}
