package website.daulac.service;

import website.daulac.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<Product> getListAllProdduct();
    Optional<Product> getProductId(Integer id);
    Product  getProductUrl(String url);
    List<Product> getListProductType(Integer id);
    List<Product> getListProductTypeName(String id);
    void saveProduct(Product product);
    void deleteProduct(Product product);
}
